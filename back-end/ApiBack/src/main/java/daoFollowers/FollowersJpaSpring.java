package daoFollowers;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Follower;

public interface FollowersJpaSpring extends JpaRepository<Follower, Integer> {

}
