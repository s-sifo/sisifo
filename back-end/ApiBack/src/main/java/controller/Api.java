package controller;



import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import daoAboutUs.AboutUsDao;
import daoContactos.ContactoDao;
import daoEventos.CategoriDAO;
import daoEventos.EventDTO;
import daoEventos.EventosDao;
import daoEventos.EventosJpaSpring;
import daoEventos.RelacionCategorias;
import daoFollowers.FollowersJpaSpring;
import daoUsuarios.ServiceUsuarios;
import daoUsuarios.UsuarioDao;
import daoUsuarios.UsuarioJpaSpring;
import model.AboutUs;
import model.Category;
import model.ContactInformation;
import model.Event;
import model.EventCategory;
import model.EventCategoryPK;
import model.FollowerPK;
import model.User;


@RestController
@RequestMapping("api")
public class Api {
	@Autowired
	AboutUsDao aboutus;
	@Autowired
	ContactoDao contacto;
	@Autowired
	UsuarioDao usuarios;
	@Autowired
	EventosDao eventos; 
	@Autowired
	UsuarioJpaSpring datosUsuarios;
	@Autowired
	EventosJpaSpring datosEventos;
	@Autowired
	FollowersJpaSpring datosFollowers;
/*	@Autowired
	CategoriDAO categorias;
	@Autowired
	RelacionCategorias relacion;
	private RelacionCategorias eventCategoryRepository;
	private EventosJpaSpring eventRepository;
	    @Autowired
	    public Api(RelacionCategorias eventCategoryRepository, EventosJpaSpring eventRepository) {
	        this.eventCategoryRepository = eventCategoryRepository;
	        this.eventRepository = eventRepository;
	    }
	*/
	
	public static String encriptPassword(String password){
		return DigestUtils.md5Hex(password);		
	}
	
	@PostMapping(value="/aboutUs",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<AboutUs> aboutus(){
		return aboutus.aboutus();
	}
	@PostMapping(value="/Contacto",produces=MediaType.APPLICATION_JSON_VALUE)
    public List<ContactInformation> contacto(){
        return contacto.obtenerContacto();
    }
	@PostMapping(value="/Usuarios",produces=MediaType.APPLICATION_JSON_VALUE)
    public List<User> obtenerUsuarios(){
        return usuarios.obtenerUsuarios();
    }
	@PostMapping(value="/Eventos",produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Event> obtenerEventos(){
        return eventos.obtenerEventos();
    }
	@PostMapping(value="/SignUp", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> addUsuario (@RequestBody User nombre) {
	    // Verificar si ya existe un usuario con el mismo correo electrónico o nombre de usuario
	    User usuarioExistente = datosUsuarios.findByEmailAndUsername(nombre.getEmail(), nombre.getUsername());
	    if (usuarioExistente != null) {
	        // Devolver una respuesta de error
	        return ResponseEntity.status(HttpStatus.CONFLICT).body("El correo electrónico o nombre de usuario ya está en uso.");
	    }
	    
	    nombre.setPassword(encriptPassword(nombre.getPassword()));
	    nombre.setIsVerified((byte) 0); // establecer como no verificado 
	    User usuarioGuardado = datosUsuarios.save(nombre);
	    return ResponseEntity.status(HttpStatus.CREATED).body(usuarioGuardado);
	}
	
	@PostMapping(value="/login", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> loginUsuario (@RequestBody User nombre) {
	    User usuario = datosUsuarios.findByEmailAndPassword(nombre.getEmail(), encriptPassword(nombre.getPassword()));
	    if (usuario == null) {
	        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
	    }
	    return ResponseEntity.status(HttpStatus.OK).body(usuario);
	}
	
	@PostMapping(value="/profile/{name}",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<User> getUsuario(@PathVariable String name) {
		return datosUsuarios.findByUsername(name);
	}
	@DeleteMapping(value="/delete/{id}")
	public void eliminarPorId(@PathVariable int id) {
			datosUsuarios.deleteById(id);
	}
	@PostMapping(value="/PorEmail/{email}",produces=MediaType.APPLICATION_JSON_VALUE)
	public User getUsuarioPorEmail(@PathVariable String email) {
		return datosUsuarios.findByEmail(email);
	}
	/*@PostMapping(value="/{categoryId}/events",produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Event> getEventsByCategoryId(@PathVariable Long category_id) {
        List<EventCategory> eventCategories = relacion.findByCategory_Id(category_id);
        List<Integer> eventIds = eventCategories.stream()
                .map(eventCategory -> eventCategory.getId().getEventId())
                .collect(Collectors.toList());

        return relacion.findAllById(eventIds);
    }*/
	//revisar el update, por ejemplo solo descripcion
	@PostMapping(value = "/CrearEvento", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> addEvento(@RequestBody EventDTO requestBody, @AuthenticationPrincipal ServiceUsuarios userDetails) {
	    int userId = requestBody.getUserId();

	    if (userId == 0) {
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ID de usuario no proporcionado");
	    }

	    Optional<User> usuarioOptional = datosUsuarios.findById(userId);
	    
	    if (usuarioOptional.isEmpty()) {
	        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Usuario no encontrado");
	    }

	    User usuario = usuarioOptional.get();

	    if (usuario.getIsVerified() == 0) {
	        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario no verificado");
	    }

	    Event evento = requestBody.getEvento();
	    evento.setUser(usuario);
	    Event eventoGuardado = datosEventos.save(evento);

	    return ResponseEntity.status(HttpStatus.CREATED).body(eventoGuardado);
	}
	@PutMapping("/verificar")
    public ResponseEntity<String> verificarUsuario(@RequestBody int id) {
        //Integer id = usuario.getUserId();
        Optional<User> optionalUsuario = datosUsuarios.findById(id);
        if (optionalUsuario.isPresent()) {
            User usuarioExistente = optionalUsuario.get();
            usuarioExistente.setIsVerified((byte) 1);
            datosUsuarios.save(usuarioExistente);
            return ResponseEntity.ok("Usuario verificado correctamente");
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	/*
	}
	/*@PostMapping("/seguir/{idUsuario}/{idUsuarioF}")
	public ResponseEntity<String> seguirUsuario(@PathVariable("idUsuario") int idUsuario, @PathVariable("idUsuarioF") int idUsuarioF) {
	    try {
	    	User seguidor = datosUsuarios.findById(idUsuario)
	    	        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado"));
	    	User seguido = datosUsuarios.findById(idUsuarioF)
	    	        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario a seguir no encontrado"));
	    	Follower follower = new Follower(seguidor, seguido);
	    	datosFollowers.save(follower);
	        return ResponseEntity.ok("Se ha seguido al usuario con éxito");
	    } catch (Exception e) {
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                .body("Ha ocurrido un error al seguir al usuario: " + e.getMessage());
	    }
	}*/
}



