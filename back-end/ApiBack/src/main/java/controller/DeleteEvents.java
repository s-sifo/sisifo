package controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import daoEventos.EventosJpaSpring;
import model.Event;

@Component
public class DeleteEvents implements CommandLineRunner {

    @Autowired
    private EventosJpaSpring eventService;
    
    //@Scheduled(cron = "0 0 0 * * *") // Ejecutar todos los días a las 12 de la noche
    
    @Override
    public void run(String... args) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date currentDate = new Date();
        System.out.println("Fecha actual: " + dateFormat.format(currentDate));

        List<Event> oldEvents = eventService.findByEventDateBefore(currentDate);
        for (Event event : oldEvents) {
            eventService.delete(event);
            System.out.println("Evento eliminado: " + event.getBody());
        }
    }
}