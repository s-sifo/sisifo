package daoEventos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import model.Category;

@Service
public interface Categoria extends JpaRepository<Category, Integer>{

	
}