package daoEventos;

import model.Event;

public class EventDTO {
    private int userId;
    private Event evento;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Event getEvento() {
		return evento;
	}
	public void setEvento(Event evento) {
		this.evento = evento;
	}
    
  
}