package daoEventos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.Event;
import model.EventCategory;

@Repository
public interface RelacionCategorias extends JpaRepository<EventCategory, Long> {

	/*List<EventCategory> findByCategory_Id(Long category_id);
    // Métodos personalizados si los necesitas

	List<Event> findAllById(List<Integer> eventIds);*/
}