package daoEventos;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import model.Event;

@Service
public interface EventosJpaSpring extends JpaRepository<Event, Integer>{

	List<Event> findByEventDateBefore(Date currentDate);
	/*List<Event> findByCategoryId(Long categoryId);
	List<Event> findByCategoria(String categoria);
	List<Event> findByCategoriaId(Long categoriaId);*/
	
}
