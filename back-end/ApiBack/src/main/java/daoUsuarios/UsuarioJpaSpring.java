package daoUsuarios;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Service;



import model.User;
import net.bytebuddy.dynamic.DynamicType.Builder.FieldDefinition.Optional;
@Service
public interface UsuarioJpaSpring extends JpaRepository<User, Integer>{
	public List<User> findByUsername(String name);
	public User findByEmailAndPassword(String name, String password);
	public User findByEmail(String email);
	public User findByEmailAndUsername(String mail, String name);
	public Optional<User> findByUserId(Long id);
}
