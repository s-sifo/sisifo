package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * The primary key class for the event_category database table.
 * 
 */
@Embeddable
public class EventCategoryPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="event_id")
	private int eventId;

	@Column(name="category_id")
	private int categoryId;
	
	

	public EventCategoryPK() {
	}
	public int getEventId() {
		return this.eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public int getCategoryId() {
		return this.categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	  
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof EventCategoryPK)) {
			return false;
		}
		EventCategoryPK castOther = (EventCategoryPK)other;
		return 
			(this.eventId == castOther.eventId)
			&& (this.categoryId == castOther.categoryId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.eventId;
		hash = hash * prime + this.categoryId;
		
		return hash;
	}
}