package model;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.*;


/**
 * The persistent class for the followers database table.
 * 
 */
@Entity
@Table(name="followers")
@NamedQuery(name="Follower.findAll", query="SELECT f FROM Follower f")
public class Follower implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private FollowerPK id;

	public Follower() {
	}

	public Follower(Optional<User> usuarioActual, Optional<User> usuarioSeguido) {
		// TODO Auto-generated constructor stub
	}

	public Follower(User seguidor, User seguido) {
		// TODO Auto-generated constructor stub
	}

	public FollowerPK getId() {
		return this.id;
	}

	public void setId(FollowerPK id) {
		this.id = id;
	}

}