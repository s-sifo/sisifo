package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the event_category database table.
 * 
 */
@Entity
@Table(name="event_category")
@NamedQuery(name="EventCategory.findAll", query="SELECT e FROM EventCategory e")
public class EventCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private EventCategoryPK id;

	public EventCategory() {
	}

	public EventCategoryPK getId() {
		return this.id;
	}

	public void setId(EventCategoryPK id) {
		this.id = id;
	}

}