package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the followers database table.
 * 
 */
@Embeddable
public class FollowerPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="user_id", insertable=true, updatable=true)
	private int userId;

	@Column(name="follower_id", insertable=true, updatable=true)
	private int followerId;

	public FollowerPK() {
	}
	public FollowerPK(int userId2, int userId3) {
		// TODO Auto-generated constructor stub
	}
	public int getUserId() {
		return this.userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getFollowerId() {
		return this.followerId;
	}
	public void setFollowerId(int followerId) {
		this.followerId = followerId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FollowerPK)) {
			return false;
		}
		FollowerPK castOther = (FollowerPK)other;
		return 
			(this.userId == castOther.userId)
			&& (this.followerId == castOther.followerId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userId;
		hash = hash * prime + this.followerId;
		
		return hash;
	}
}