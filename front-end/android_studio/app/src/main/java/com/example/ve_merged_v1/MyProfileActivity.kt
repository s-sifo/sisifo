package com.example.ve_merged_v1

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.ve_merged_v1.databinding.ActivityMyProfileBinding
import com.example.ve_merged_v1.retrofit.callPostVerificar
import com.example.ve_merged_v1.retrofit.retrofit
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.launch


class MyProfileActivity : AppCompatActivity() {
    //dd toolbar logic
    lateinit var ddFab: FloatingActionButton
    lateinit var ddMenu: CardView
    lateinit var ddMBGoToMyProfile: Button
    var ddOpen = false

    private lateinit var binding: ActivityMyProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMyProfileBinding.inflate(layoutInflater)
        //setContentView(R.layout.activity_main)
        setContentView(binding.root)
        /* Toolbar block */
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false);
        supportActionBar?.setDisplayShowHomeEnabled(false);
//        ddFab = findViewById(R.id.toolbar_fabHome)
//        ddFab.setImageResource(R.drawable.fab_gradient_reload)
//        ddFab.hide()

        /* CardView of Buttons */
        ddMenu = findViewById(R.id.toolbar_dropDownCard)

        /// Hide elements on menu
                ddMBGoToMyProfile = findViewById(R.id.toolbar_btnGoToMyProfile)
                ddMBGoToMyProfile.isVisible = false

//        /* Load user and build View block */
        val global : GlobalPreferences = GlobalPreferences.getInstance()
        val lUser = global.logedUser
        //Glide.with(binding.infoIvEventImage).load(EventProvider.events.get(index).image).into(binding.infoIvEventImage);
        if (lUser.profilePicture.isNullOrBlank()){
            Glide.with(binding.profileIvPfp).load(R.drawable.ic_default_pfp_svg).into(binding.profileIvPfp);
        }

        binding.profileTvUserName.text = lUser.username
        binding.profileTvJoinDate.append(lUser.creationDate)
        if (lUser.verified){
            binding.profileTvVerification.append(" Verified")
        }else{
            binding.profileTvVerification.append(" Not verified")

        }

        val btn : ImageView = findViewById(R.id.profile_ivVerification)

        btn.setOnClickListener {
            if (lUser.verified == true) {
                return@setOnClickListener
            }
            lifecycleScope.launch {

                if(callPostVerificar(lUser.userId)) {
                    binding.profileTvVerification.text = " Verified"

                    global.logedUser.verified = true

                }
            }
        }


    }

    fun openDropDown(view: View) {

        ddFab = findViewById(R.id.toolbar_fabDropdown)
        if (ddOpen) {
            ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown))
            ddMenu.visibility = View.INVISIBLE
            ddOpen = false
            //button closed after
        } else {
            ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown_open))
            ddOpen = true //TODO closed with elevation shadow
            ddMenu.visibility = View.VISIBLE
            ddMenu.setFocusableInTouchMode(true)
            ddMenu.requestFocus()
            ddMenu.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown_open))
                    ddOpen = true
                } else {
                    ddMenu.visibility = View.INVISIBLE
                    ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown))
                    ddOpen = false
                }
            }
            //button open after
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.dd_option2) {
//            Toast.makeText(this, "clicked on dd menu", Toast.LENGTH_SHORT).show()
        }
        closeOptionsMenu()
        return super.onOptionsItemSelected(item)
    }

    fun openRegisterActivity(view: View) {
        val intent = Intent(this, RegisterActivity::class.java)
//        Toast.makeText(this, "going to register activity", Toast.LENGTH_SHORT).show()
        startActivity(intent)
        ddMenu.visibility = View.INVISIBLE
    }

    fun goToEventFeed(view: View) {
        val intent = Intent(this, EventFeedActivity::class.java)
//        Toast.makeText(this, "going to event feed activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
            ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun goToMyProfile(view: View) { //TODO gotos
        val intent = Intent(this, MyProfileActivity::class.java)
//        Toast.makeText(this, "going to my profile activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun goToAboutUs(view: View) { //TODO gotos
        val intent = Intent(this, AboutUsActivity::class.java)
//        Toast.makeText(this, "going to about us activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun closeSession(view: View) { //TODO gotos
        val intent = Intent(this, MainActivity::class.java)
//        Toast.makeText(this, "going to main activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

}


