package com.example.ve_merged_v1

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ve_merged_v1.dataStructure.Event
import com.example.ve_merged_v1.eventsVh.EventsRvAdapter
import com.example.ve_merged_v1.retrofit.callGetEvents
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat


class EventFeedActivity : AppCompatActivity() {
    lateinit var rvEvents: RecyclerView
    lateinit var ddFab: FloatingActionButton
    lateinit var ddMenu: CardView
    lateinit var ddMBGoToEvents: Button
    var ddOpen: Boolean = false
    var events: List<Event> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val policy = ThreadPolicy.Builder().permitAll().build() //todo mirar esto, prohibia api
        StrictMode.setThreadPolicy(policy)

        setContentView(R.layout.activity_event_feed)

        // Recycler view block
//        rvEvents = findViewById(R.id.rvEvents)
//        rvEvents.layoutManager = LinearLayoutManager(this)
//        // rvEvents.adapter = EventsRvAdapter(EventProvider.events)
//        rvEvents.adapter = EventsRvAdapter(events)

        /* Toolbar */
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false);
        supportActionBar?.setDisplayShowHomeEnabled(false);
        ddFab = findViewById(R.id.toolbar_fabHome)
        ddFab.setImageResource(R.drawable.fab_gradient_reload)
        /* Toolbar Dropdown */
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        /// Hide elements on menu
        ddMBGoToEvents = findViewById(R.id.toolbar_btnGoToEvents)
        ddMBGoToEvents.isVisible = false

//        event = EventProvider.events.get(eventsViewHolder.adapterPosition)
//        var tvRvTitle = findViewById<TextView>(R.id.tvRvTitle)
//        tvRvTitle = findViewById(R.id.tvRvTitle)


        //TODO fijar barra superior eventFeed
        /* API call */
        lifecycleScope.launch{
            events = callGetEvents()
            onResultBuildEventsRV(events)
//            goToDetailsWithApiData(events)
        }

//        GlobalScope.launch{
////            var events =  callGetEvents()
//            var events: List<Event> = callGetEvents()
//            onResultBuildView(events)
//        }


    }

    private suspend fun onResultBuildEventsRV(events: List<Event>){
        rvEvents = findViewById(R.id.rvEvents)
        rvEvents.layoutManager = LinearLayoutManager(this)
//        var dateList: ArrayList<Date> = listOf()
        for (event in events) {
            if (event.eventDate.isNullOrEmpty()) {event.eventDate = ""}
//            if (event.eventHour == null){event.eventHour = ""}
            // event.eventId
            //TODO get links
//            if (event.image.contains("http")) else { event.image = "https://soporte.mygestion.com/media/wp-content/uploads/tipos-de-stock-700x394.jpg" }
//            event.body
//            if (event.body == null){event.body = ""}
//            // event.creationDate
//            if (event.location == null){event.location = ""}
//            // event.title
//            events.sortedByDescending { event.eventDate }
            var parsedDate = SimpleDateFormat("yyyy-MM-dd").parse(event.eventDate)
//            events.sortedByDescending {parsedDate}
            println(parsedDate)
//            dateList.add(parsedDate)
        }
//        this.events = events.sortedByDescending { it.eventDate }
//        events.sortedWith(Date.)

        rvEvents.adapter = EventsRvAdapter(events.sortedBy { it.eventDate })
        this.events = events.sortedBy { it.eventDate }
    }

    fun openCreateEventActivity(view: View) {
        //val input = findViewById<EditText>(R.id.etEmail)
        //val userEmail = input.text;
        val intent = Intent(this, CreateEventActivity::class.java)
//        Toast.makeText(this, "going to create event activity", Toast.LENGTH_SHORT).show()
        startActivity(intent)
        //println("Hello world")
    }

    fun goToEventFeed(view: View) {
        //Reload logic
        recreate()
    }

    fun openEventDetailsActivity(view: View) { //todo eventinfo
        val intent = Intent(this, EventDetailsActivity::class.java)
//        Toast.makeText(this, "INDEX: " + view.tag, Toast.LENGTH_SHORT).show()
        var index:Int = Integer.parseInt(view.tag.toString())
        intent.putExtra("index", index)
        intent.putExtra("title", events.get(index).title)
        intent.putExtra("body", events.get(index).body)
        intent.putExtra("image", events.get(index).image)
        intent.putExtra("eventDate", events.get(index).eventDate)
        intent.putExtra("creationDate", events.get(index).creationDate)
        intent.putExtra("location", events.get(index).location)
        intent.putExtra("eventHour", events.get(index).eventHour)
        startActivity(intent)
    }

    fun openDropDown(view: View) {

        ddFab = findViewById(R.id.toolbar_fabDropdown)
        if (ddOpen) {
            ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown))
            ddMenu.visibility = View.INVISIBLE
            ddOpen = false
            //button closed after
        } else {
            ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown_open))
            ddOpen = true //TODO closed with elevation shadow
            ddMenu.visibility = View.VISIBLE
            ddMenu.setFocusableInTouchMode(true)
            ddMenu.requestFocus()
            ddMenu.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown_open))
                    ddOpen = true
                } else {
                    ddMenu.visibility = View.INVISIBLE
                    ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown))
                    ddOpen = false
                }
            }
            //button open after
        }
    }

    fun goToMyProfile(view: View) { //TODO gotos
        val intent = Intent(this, MyProfileActivity::class.java)
//        Toast.makeText(this, "going to my profile activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun goToAboutUs(view: View) { //TODO gotos
        val intent = Intent(this, AboutUsActivity::class.java)
//        Toast.makeText(this, "going to about us activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun closeSession(view: View) { //TODO gotos
        val intent = Intent(this, MainActivity::class.java)
//        Toast.makeText(this, "going to main activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

}