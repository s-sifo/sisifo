package com.example.ve_merged_v1.dataStructure

class EventProvider {
    companion object {
        var events = listOf<Event>(
            Event(
                "TITULO 1",
                "Nulla vestibulum, felis sed scelerisque accumsan, lacus tortor aliquam lorem, id elementum mi lacus ac ligula.",
                "https://images.ctfassets.net/hrltx12pl8hq/2JVN7glHv8PRyML5X6m20u/bcaec97f32ad79621948e16fc5c130a9/shutterstock_1355665841.jpg",
                "30-04-2023",
                "20-7-2023",
                "https://www.foo.bar.bar.foo.com/event1url/",
                "20:00"
            ),
            Event(
                "TITULO 2",
                "Nulla vestibulum, felis sed scelerisque accumsan, lacus tortor aliquam lorem, id elementum mi lacus ac ligula. Aenean placerat at mauris quis tristique. Maecenas molestie tincidunt vestibulum. Sed pretium euismod diam. Proin pellentesque elementum sodales. Integer at aliquet ante. Duis ut nisi pharetra, efficitur justo ullamcorper, malesuada ex. Nam et pretium mauris.",
                "https://images.ctfassets.net/hrltx12pl8hq/6HqySCb3zseJdpfaTAxs6X/ed59d8e516cec09a91eaa835cf34a5d3/shutterstock_539932924.jpg",
                "06-05-2023",
                "10-3-2023",
                "url/web",
                "03:40"
            ),
            Event(
                "TITULO 3",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/242KzrtS2dMIymxdzutWkR/0b1658936d115d7057cadf9cf8c519e8/shutterstock_1043633563.jpg",
                "28-05-2023",
                "10-3-2023",
                "url/web",
                "22:00"
            ),
            Event(
                "Cuarto Titulo de Evento",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/2V0fezqZrIn5YLO6K8ZYrh/10e3a46b609899beba079aecd8195cc3/shutterstock_2015729612.jpg",
                "03-06-2023",
                "10-3-2023",
                "url/web",
                "20:00"
            ),
            Event(
                "Quinto Titulo de Evento",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/7G8oMV6lNcznsPUWNNHvMH/199d649c57652ccc08db23f46a485207/Oct_thumbnail_03.jpg",
                "08-07-2023",
                "10-3-2023",
                "url/web",
                "20:00"
            ),
            Event(
                "Sexto Titulo de Evento",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/6faMpUY0LMolAN7ws0ln9e/aab52c1ae29684dc8a7ae9c0d2a3ada5/thumb_july22_02.jpg",
                "02-07-2023",
                "10-3-2023",
                "url/web",
                "20:00"
            ),
            Event(
                "Septimo Titulo de Evento",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/3Zc8Iycp6TKLLz0bxcmP2s/a4a537db71bd55f2f58ae74d4508e63b/Frame_41.jpg",
                "02-07-2023",
                "10-3-2023",
                "url/web",
                "20:00"

            )
        )
        var events2 = listOf<Event>(
            Event(
                "EVENTS2 LIST",
                "EVENTS2 felis sed scelerisque accumsan, lacus tortor aliquam lorem, id elementum mi lacus ac ligula.",
                "https://images.ctfassets.net/hrltx12pl8hq/2JVN7glHv8PRyML5X6m20u/bcaec97f32ad79621948e16fc5c130a9/shutterstock_1355665841.jpg",
                "30-04-2023",
                "20-7-2023",
                "https://www.foo.bar.bar.foo.com/event1url/",
                "20:00"
            ),
            Event(
                "TITULO 2",
                "Nulla vestibulum, felis sed scelerisque accumsan, lacus tortor aliquam lorem, id elementum mi lacus ac ligula. Aenean placerat at mauris quis tristique. Maecenas molestie tincidunt vestibulum. Sed pretium euismod diam. Proin pellentesque elementum sodales. Integer at aliquet ante. Duis ut nisi pharetra, efficitur justo ullamcorper, malesuada ex. Nam et pretium mauris.",
                "https://images.ctfassets.net/hrltx12pl8hq/6HqySCb3zseJdpfaTAxs6X/ed59d8e516cec09a91eaa835cf34a5d3/shutterstock_539932924.jpg",
                "06-05-2023",
                "10-3-2023",
                "url/web",
                "03:40"
            ),
            Event(
                "TITULO 3",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/242KzrtS2dMIymxdzutWkR/0b1658936d115d7057cadf9cf8c519e8/shutterstock_1043633563.jpg",
                "28-05-2023",
                "10-3-2023",
                "url/web",
                "22:00"
            ),
            Event(
                "Cuarto Titulo de Evento",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/2V0fezqZrIn5YLO6K8ZYrh/10e3a46b609899beba079aecd8195cc3/shutterstock_2015729612.jpg",
                "03-06-2023",
                "10-3-2023",
                "url/web",
                "20:00"
            ),
            Event(
                "Quinto Titulo de Evento",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/7G8oMV6lNcznsPUWNNHvMH/199d649c57652ccc08db23f46a485207/Oct_thumbnail_03.jpg",
                "08-07-2023",
                "10-3-2023",
                "url/web",
                "20:00"
            ),
            Event(
                "Sexto Titulo de Evento",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/6faMpUY0LMolAN7ws0ln9e/aab52c1ae29684dc8a7ae9c0d2a3ada5/thumb_july22_02.jpg",
                "02-07-2023",
                "10-3-2023",
                "url/web",
                "20:00"
            ),
            Event(
                "Septimo Titulo de Evento",
                "Hello world evento evento evento.",
                "https://images.ctfassets.net/hrltx12pl8hq/3Zc8Iycp6TKLLz0bxcmP2s/a4a537db71bd55f2f58ae74d4508e63b/Frame_41.jpg",
                "02-07-2023",
                "10-3-2023",
                "url/web",
                "20:00"

            )
        )
    }
}