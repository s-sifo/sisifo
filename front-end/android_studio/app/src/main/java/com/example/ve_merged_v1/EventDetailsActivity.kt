package com.example.ve_merged_v1

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.example.ve_merged_v1.databinding.ActivityDetailsEventBinding
import com.google.android.material.floatingactionbutton.FloatingActionButton

class EventDetailsActivity : AppCompatActivity() {
    lateinit var ddFab: FloatingActionButton
    lateinit var ddMenu: CardView
    var ddOpen = false
    lateinit var ddMBGoToEvents: Button

    private lateinit var binding: ActivityDetailsEventBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var index = intent.getIntExtra("index",0)
        var id = intent.getIntExtra("eventId",0)
        var title = intent.getStringExtra("title")
        var body = intent.getStringExtra("body")
        var image = intent.getStringExtra("image")
        var eventDate = intent.getStringExtra("eventDate")
        var creationDate = intent.getStringExtra("creationDate")
        var location = intent.getStringExtra("location")
        var eventHour = intent.getStringExtra("eventHour")

        setContentView(R.layout.activity_details_event)
        binding = ActivityDetailsEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /* Toolbar */
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false);
        supportActionBar?.setDisplayShowHomeEnabled(false);
        ddFab = findViewById(R.id.toolbar_fabHome)
        ddFab.setImageResource(R.drawable.fab_gradient_back)
        /* Toolbar Dropdown */
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        /// Hide elements on menu
        ddMBGoToEvents = findViewById(R.id.toolbar_btnGoToEvents)
        ddMBGoToEvents.isVisible = false

        /* Element Loading */ // from API
        Glide.with(binding.infoIvEventImage).load(image).into(binding.infoIvEventImage);
        binding.detailsTvTitle.text =  title
        binding.detailsTvEventInfo.text =  body
        binding.detailsTvEventDate.text =  eventDate
        binding.detailsTvEventLoc.text =  location
        binding.detailsTvEventTime.text =  eventHour

        /* Element Loading */ // from EventProvider
//        Glide.with(binding.infoIvEventImage).load(EventProvider.events.get(index).image).into(binding.infoIvEventImage);
//        binding.detailsTvTitle.text =  EventProvider.events.get(index).title
//        binding.detailsTvEventInfo.text =  EventProvider.events.get(index).body
//        binding.detailsTvEventDate.text =  EventProvider.events.get(index).eventDate
//        binding.detailsTvEventLoc.text =  EventProvider.events.get(index).location
//        binding.detailsTvEventTime.text =  EventProvider.events.get(index).eventHour

    }

    fun openDropDown(view: View) {

        ddFab = findViewById(R.id.toolbar_fabDropdown)
        if (ddOpen) {
            ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown))
            ddMenu.visibility = View.INVISIBLE
            ddOpen = false
            //button closed after
        } else {
            ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown_open))
            ddOpen = true //TODO closed with elevation shadow
            ddMenu.visibility = View.VISIBLE
            ddMenu.setFocusableInTouchMode(true)
            ddMenu.requestFocus()
            ddMenu.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown_open))
                    ddOpen = true
                } else {
                    ddMenu.visibility = View.INVISIBLE
                    ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown))
                    ddOpen = false
                }
            }
            //button open after
        }
    }

    fun goToEventFeed(view: View) {
        val intent = Intent(this, EventFeedActivity::class.java)
//        Toast.makeText(this, "going to event feed activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        if (ddMenu != null){
            ddMenu.visibility = View.INVISIBLE
        }
        startActivity(intent)
    }
    fun goToMyProfile(view: View) { //TODO gotos
        val intent = Intent(this, MyProfileActivity::class.java)
//        Toast.makeText(this, "going to my profile activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun goToAboutUs(view: View) { //TODO gotos
        val intent = Intent(this, AboutUsActivity::class.java)
//        Toast.makeText(this, "going to about us activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun closeSession(view: View) { //TODO gotos
        val intent = Intent(this, MainActivity::class.java)
//        Toast.makeText(this, "going to main activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }
}

