package com.example.vesecond.connection

import com.example.ve_merged_v1.retrofit.APIService
import com.example.ve_merged_v1.retrofit.Routes
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection
import java.net.URL

class Connection {

    val retrofit = Retrofit.Builder()
        .baseUrl(Routes.UrlBase)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val apiService = retrofit.create(APIService::class.java)

    fun conn(): HttpURLConnection {

        val url = URL("http://localhost:1113/api/")
        val connection = url.openConnection() as HttpURLConnection;

        connection.requestMethod = "POST"

        connection.setRequestProperty("Content-Type", "application/json")
        connection.setRequestProperty("Accept", "application/json")

        return connection
    }

}
