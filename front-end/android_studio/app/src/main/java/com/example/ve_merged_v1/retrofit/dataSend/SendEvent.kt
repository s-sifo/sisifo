package com.example.ve_merged_v1.retrofit.dataSend

data class SendEvent
    (
    val title:String,
    var body:String,
    var image:String?,
    var eventDate:String,
    var location:String,
    var eventHour:String
)