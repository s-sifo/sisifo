package com.example.ve_merged_v1

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import com.example.ve_merged_v1.databinding.ActivityMainBinding
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.*


class MainActivity : AppCompatActivity() {
    //dd toolbar logic
    lateinit var ddFab: FloatingActionButton
    lateinit var ddMenu: CardView
    var ddOpen = false

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        /* Toolbar block */
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false);
        supportActionBar?.setDisplayShowHomeEnabled(false);

        /* CardView of Buttons */
        ddMenu = findViewById(R.id.toolbar_dropDownCard)


        /* Cargar idioma desde shared preferences */
        val config = resources.configuration
        val prefs = getSharedPreferences("com.example.VEsecond.myapplication", Context.MODE_PRIVATE)
        val lang = prefs.getString("lang", "")
        var newLocale: Locale
        if (lang == "en_us" || lang == "en"){
            binding.mainIvClBtn.setImageResource(R.drawable.ic_en_flag)
            newLocale = Locale("en")

        }else{
            binding.mainIvClBtn.setImageResource(R.drawable.ic_es_flag)
            newLocale = Locale("es")
        }
        config.setLocale(newLocale)
        prefs.edit().putString("lang", newLocale.toString()).apply()
    }

    fun openDropDown(view: View) {
        ddFab = findViewById(R.id.toolbar_fabDropdown)
        if (ddOpen) {
            ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown))
            ddMenu.visibility = View.INVISIBLE
            ddOpen = false
            //button closed after
        } else {
            ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown_open))
            ddOpen = true //TODO closed with elevation shadow
            ddMenu.visibility = View.VISIBLE
            ddMenu.setFocusableInTouchMode(true)
            ddMenu.requestFocus()
            ddMenu.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown_open))
                    ddOpen = true
                } else {
                    ddMenu.visibility = View.INVISIBLE
                    ddFab.setImageDrawable(getDrawable(R.drawable.fab_gradient_dropdown))
                    ddOpen = false
                }
            }
            //button open after
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.dd_option2) {
//            Toast.makeText(this, "clicked on dd menu", Toast.LENGTH_SHORT).show()
        }
        closeOptionsMenu()
        return super.onOptionsItemSelected(item)
    }

    fun openLoginActivity(view: View) {
            val intent = Intent(this, LoginActivity::class.java)
//            Toast.makeText(this, "going to login activity", Toast.LENGTH_SHORT).show()
            startActivity(intent)

            ddMenu.visibility = View.INVISIBLE

    }

    fun openRegisterActivity(view: View) {
        val intent = Intent(this, RegisterActivity::class.java)
//        Toast.makeText(this, "going to register activity", Toast.LENGTH_SHORT).show()
        startActivity(intent)
        ddMenu.visibility = View.INVISIBLE
    }

    fun goToEventFeed(view: View) {
        val intent = Intent(this, EventFeedActivity::class.java)
//        Toast.makeText(this, "going to event feed activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun goToMyProfile(view: View) { //TODO gotos
        val intent = Intent(this, MyProfileActivity::class.java)
//        Toast.makeText(this, "going to my profile activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun goToAboutUs(view: View) { //TODO gotos
        val intent = Intent(this, AboutUsActivity::class.java)
//        Toast.makeText(this, "going to about us activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun closeSession(view: View) { //TODO gotos
        val intent = Intent(this, MainActivity::class.java)
//        Toast.makeText(this, "going to main activity", Toast.LENGTH_SHORT).show()
        ddMenu = findViewById(R.id.toolbar_dropDownCard)
        ddMenu.visibility = View.INVISIBLE
        startActivity(intent)
    }

    fun changeLanguage(view: View) { //todo testear
        val prefs = getSharedPreferences("com.example.VEsecond.myapplication", Context.MODE_PRIVATE)
        val lang = prefs.getString("lang", "")
        val config = resources.configuration
        var newLocale: Locale
        println(Locale.getDefault().toString())
        if (lang == "en_us" || lang == "en"){
            newLocale = Locale("es")
            binding.mainIvClBtn.setImageResource(R.drawable.ic_es_flag)
        }else{
            newLocale = Locale("en")
            binding.mainIvClBtn.setImageResource(R.drawable.ic_en_flag)
        }
        config.setLocale(newLocale)
        prefs.edit().putString("lang", newLocale.toString()).apply()
    }

}


