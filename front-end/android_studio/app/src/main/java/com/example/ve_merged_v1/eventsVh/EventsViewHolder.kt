package com.example.ve_merged_v1.eventsVh

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ve_merged_v1.dataStructure.Event
import com.example.ve_merged_v1.databinding.EventRvLayoutBinding
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class EventsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val binding = EventRvLayoutBinding.bind(view)

    fun printEvent(event: Event) {
        binding.rvConstraintLayoutEventMain.tag = adapterPosition.toString()
        binding.tvRvTitle.text = event.title
        binding.tvRvDescription.text = event.body
        binding.tvRvEventTime.text = event.eventHour.substring(0,5)
        /* Image loading */
        Glide.with(binding.rvIvEventImage).load(event.image).into(binding.rvIvEventImage);
        /* Logic to format date string */
        if (event.eventDate.isNullOrBlank()){
            print("Error: Date is null or blank")
        }else {
            var parsedDate = SimpleDateFormat("MM-dd").parse(event.eventDate.substring(5, 10))
            var ddMMMDateStr = SimpleDateFormat("MMM-dd").format(parsedDate)
            ddMMMDateStr = ddMMMDateStr.replace("-", " ")
            binding.tvRvEventDate.text = ddMMMDateStr
        }
        /* Logic for date offset */ // YYYY-MM-DD version
        var parsedDateBig = SimpleDateFormat("yyyy-MM-dd").parse(event.eventDate)
        var dateOffsetMs = parsedDateBig.getTime() - Date().getTime()
        var dateOffsetDays = TimeUnit.DAYS.convert(dateOffsetMs, TimeUnit.MILLISECONDS)
        if (dateOffsetDays < 0){
            binding.rvConstraintLayoutEventMain.backgroundTintList = ColorStateList.valueOf(Color.RED);
            binding.tvRvEventDateOffset.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#FF9986"));
            binding.tvRvEventDate.backgroundTintList = ColorStateList.valueOf(Color.RED);
            binding.tvRvEventDateOffset.text = dateOffsetDays.toString() + "d"
        } else if (dateOffsetDays < 30) {
            binding.tvRvEventDateOffset.text = "+" + dateOffsetDays.toString() + "d"
        } else {
            binding.rvConstraintLayoutEventMain.backgroundTintList = ColorStateList.valueOf(Color.MAGENTA);
            binding.tvRvEventDateOffset.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#BD7BFF"));
            binding.tvRvEventDate.backgroundTintList = ColorStateList.valueOf(Color.MAGENTA);
            dateOffsetDays = dateOffsetDays / 30
            binding.tvRvEventDateOffset.text = "~" + dateOffsetDays.toString() + "m"
        }
    }
}