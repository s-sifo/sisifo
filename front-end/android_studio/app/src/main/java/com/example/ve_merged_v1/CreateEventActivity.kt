package com.example.ve_merged_v1

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.ve_merged_v1.retrofit.callSetEvent
import com.example.ve_merged_v1.retrofit.dataSend.SendEvent
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream

class CreateEventActivity : AppCompatActivity() {

    var imageUri:Uri = Uri.parse("");
    val codePickImg=190;

    lateinit var blob: ByteArray

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_event)

        val titulo : EditText = findViewById(R.id.eventCreate_etName)
        val descripcion : EditText = findViewById(R.id.eventCreate_etDescription)
        val lugar : EditText = findViewById(R.id.eventCreate_etPlace)
        val fecha : EditText = findViewById(R.id.eventCreate_etDate)
        val hora : EditText = findViewById(R.id.eventCreate_etTime)
        val btnSelectImage : Button = findViewById(R.id.eventCreate_btnSelectImage)

        val btnCrearEvento : ImageButton = findViewById(R.id.eventCreate_btnAdd)

        btnSelectImage.setOnClickListener {
            selectImage()
        }

        btnCrearEvento.setOnClickListener{

            if (titulo.text.toString().isEmpty() && descripcion.text.toString().isEmpty() && lugar.text.toString().isEmpty() && fecha.text.toString().isEmpty() && hora.text.toString().isEmpty()){
                println("Failed -- empty")
                return@setOnClickListener
            }

            println("Verificacion pasada")

            val global : GlobalPreferences = GlobalPreferences.getInstance()

            println(global.logedUser.verified)


            val event : SendEvent = SendEvent(titulo.text.toString(),descripcion.text.toString(),null,fecha.text.toString(), lugar.text.toString(),hora.text.toString())

            lifecycleScope.launch{
                popUp(callSetEvent(global.logedUser.userId, event))

            }

        }
    }

    fun popUp(pop : Boolean) {

        val rootView = findViewById<View>(android.R.id.content)
        val message : String
        val duration = Snackbar.LENGTH_LONG

        if (pop) {
            message  = "Evento creado con exito"
            val intent = Intent(this, EventFeedActivity::class.java)
            startActivity(intent)
        } else {
            message = "Usuario no verificado"
        }

        val snackbar = Snackbar.make(rootView, message, duration)

        snackbar.show()
    }

    fun selectImage() {
        val galery= Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(galery,190)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode== RESULT_OK && requestCode == codePickImg){
            imageUri = data?.data!!


            val bitmap: Bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(imageUri.toString()))


            val toBlob : ImageView = findViewById(R.id.eventCreate_imgPhoto)
            toBlob.setImageBitmap(bitmap)

            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos)
            blob = bos.toByteArray()

            println(blob)
            println(blob.toString())
        }
    }

}