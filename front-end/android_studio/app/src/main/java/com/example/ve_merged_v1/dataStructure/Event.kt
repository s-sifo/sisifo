package com.example.ve_merged_v1.dataStructure

data class Event
    (
    val title:String,
    var body:String,
    var image:String?,
    var eventDate:String,
    val creationDate:String?,
    var location:String,
    var eventHour:String
    )
