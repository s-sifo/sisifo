package com.example.ve_merged_v1.retrofit

import com.example.ve_merged_v1.dataStructure.Event
import com.example.ve_merged_v1.dataStructure.User
import com.example.ve_merged_v1.retrofit.dataSend.SendEvent
import com.example.ve_merged_v1.retrofit.dataSend.SendLoginUser
import com.example.ve_merged_v1.retrofit.dataSend.SendRegisterUser
import com.example.ve_merged_v1.retrofit.dataSend.SendSendEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val retrofit = Retrofit.Builder()
    .baseUrl(Routes.UrlBase)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

        // Llama a postRegister, le pasa un objeto sendLoginUser y si la respuesta es exitosa devuelve true, sino false
suspend fun callPostRegister(user : SendRegisterUser): Boolean {

    val apiService: APIService = retrofit.create(APIService::class.java)
    val call : Call<Void> = apiService.postRegister(user)
    val response : Response<Void> = call.execute()

    var successful : Boolean = false

    withContext(Dispatchers.Main) {
        if (response.isSuccessful) {
            println(response.body())
            successful = true

        }
    }
    return successful;
}

        // Llama a postLogin para loguearse, envia un objeto sendRegisterUser y si la respuesta es exitosa manda un objeto User sino un null
suspend fun callPostLogin(user : SendLoginUser) : User? {

            var userLogged: User? = null

            val apiService: APIService = retrofit.create(APIService::class.java)
            val call: Call<User> = apiService.postLogin(user)
            val response: Response<User> = call.execute()

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    val userId: Int = response.body()!!.userId
                    val closingDate: String? = response.body()!!.closingDate
                    val creationDate: String = response.body()!!.creationDate
                    val description: String? = response.body()!!.description
                    val email:String = response.body()!!.email
                    val verified:Boolean = response.body()!!.verified
                    val password:String = response.body()!!.password
                    val profilePicture:String? = response.body()!!.profilePicture
                    val username: String = response.body()!!.username
                    val events: ArrayList<Event> = response.body()!!.events

                    userLogged = User(userId, closingDate, creationDate, description, email, verified, password, profilePicture, username, events)


                } else {

                }
            }
            return userLogged
}

// Llama a postUpdateUser, envia un objeto User y si la respuesta es exitosa devuelve el usuario con datos cambiados, sino, devuelve el viejo
suspend fun callUpdateUser(user : User) : User? {

    val apiService: APIService = retrofit.create(APIService::class.java)
    val call : Call<User> = apiService.postUpdateUser(user)
    val response : Response<User> = call.execute()

    withContext(Dispatchers.Main) {
        if (response.isSuccessful) {
            println(response.body())
        } else {

        }
    }
    return null
}

suspend fun callPostVerificar(id : Int): Boolean {

    val apiService: APIService = retrofit.create(APIService::class.java)
    val call : Call<Void> = apiService.postVerificar(id)
    val response : Response<Void> = call.execute()

    var verificado : Boolean = false

    withContext(Dispatchers.Main) {

        verificado = response.isSuccessful
        println(response.isSuccessful)
    }
    return verificado
}

suspend fun callSetEvent(id: Int, event: SendEvent) : Boolean {

    var respuesta : Boolean

    val marmol : SendSendEvent = SendSendEvent(event)

    val apiService: APIService = retrofit.create(APIService::class.java)
    val call : Call<Void> = apiService.postSetEvent(id, marmol)
    val response : Response<Void> = call.execute()

    withContext(Dispatchers.Main) {
        respuesta = response.isSuccessful
        if (response.isSuccessful) {
            respuesta = response.isSuccessful
        } else {
            println("---->> error <<----")
            println(response.errorBody())
        }
    }
    return respuesta
}

suspend fun callGetEvents() : List<Event> {        //TODO: Comprobar que funciona
    println()
//    var Events : List<Event> = List<Event>()
    var events = listOf<Event>()
    val apiService: APIService = retrofit.create(APIService::class.java)
    val call : Call<List<Event>> = apiService.postGetEvent()
    val response : Response<List<Event>> = call.execute()

    withContext(Dispatchers.Main) {
        if (response.isSuccessful) {
            events = response.body()!!
        } else {
            println("---->> error <<----")
        }
    }
    return events

}
