package com.example.ve_merged_v1

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import com.example.ve_merged_v1.dataStructure.User
import com.example.ve_merged_v1.retrofit.callPostLogin
import com.example.ve_merged_v1.retrofit.dataSend.SendLoginUser
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoginActivity : AppCompatActivity() {


    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        lateinit var tbFab: FloatingActionButton
        lateinit var ddFab: FloatingActionButton
        lateinit var ddMenu: CardView

        var ddOpen = false

        val inputEmail : EditText = findViewById(R.id.login_etEmail)
        val inputPassw : EditText = findViewById(R.id.login_etPassw)

        val btnLogin : Button = findViewById(R.id.login_btnLogin)

        btnLogin.setOnClickListener{

            if (inputEmail.text.toString().isEmpty() && inputPassw.text.toString().isEmpty()){
                println("Failed -- empty")
                return@setOnClickListener
            }

            var user = SendLoginUser(inputEmail.text.toString(),inputPassw.text.toString())

            GlobalScope.launch {

                val global : GlobalPreferences = GlobalPreferences.getInstance()
                var logedUser : User? = callPostLogin(user)

                if (logedUser == null){
                    popUp(false)
                } else {
                    global.logedUser = logedUser
                    println(global.logedUser)
                    popUp(true)
                }

            }
        }

        /* Toolbar */
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false);
        supportActionBar?.setDisplayShowHomeEnabled(false);
        tbFab = findViewById(R.id.toolbar_fabHome)
        tbFab.setImageResource(R.drawable.fab_gradient_back)
        /* Toolbar Dropdown */
        ddFab = findViewById(R.id.toolbar_fabDropdown)
        ddMenu = findViewById(R.id.toolbar_dropDownCard)

        /// Hide dd menu
        ddMenu.isVisible = false
        ddFab.isVisible = false
    }

    //gotomain
    fun goToEventFeed(view: View) {
        val intent = Intent(this, MainActivity::class.java)
//        Toast.makeText(this, "going to event feed activity", Toast.LENGTH_SHORT).show()
        startActivity(intent)
    }

    fun popUp(pop : Boolean) {

        val rootView = findViewById<View>(android.R.id.content)
        val message : String
        val duration = Snackbar.LENGTH_LONG

        if (pop) {
            message  = "Usuario creado con exito"
            val intent = Intent(this, EventFeedActivity::class.java)
            startActivity(intent)
        } else {
            message = "Fallo al loggear"
        }

        val snackbar = Snackbar.make(rootView, message, duration)

        snackbar.show()
    }
}