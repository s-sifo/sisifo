package com.example.ve_merged_v1.dataStructure

data class User(
    val userId: Int,
    val closingDate: String?,
    val creationDate: String,
    val description: String?,
    val email:String,
    var verified:Boolean,
    val password:String,
    val profilePicture:String?,
    val username: String,
    val events: ArrayList<Event>,
)
