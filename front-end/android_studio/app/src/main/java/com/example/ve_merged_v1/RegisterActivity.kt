package com.example.ve_merged_v1

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import com.example.ve_merged_v1.retrofit.callPostRegister
import com.example.ve_merged_v1.retrofit.dataSend.SendRegisterUser
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RegisterActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        lateinit var tbFab: FloatingActionButton
        lateinit var ddFab: FloatingActionButton
        lateinit var ddMenu: CardView

        val inputUser : EditText = findViewById(R.id.register_etUser)
        val inputEmail : EditText = findViewById(R.id.register_etEmail)
        val inputPassw : EditText = findViewById(R.id.register_etPassw)

        val btnRegister : Button = findViewById(R.id.register_btnRegister)

        btnRegister.setOnClickListener{

            // Si los campos estan vacios, no procede a registrar
            if (inputUser.text.toString().isEmpty() && inputEmail.text.toString().isEmpty() && inputPassw.text.toString().isEmpty()){
                println("Failed -- empty")
                return@setOnClickListener
            }

            var user = SendRegisterUser(inputUser.text.toString(),inputEmail.text.toString(),inputPassw.text.toString())

            GlobalScope.launch {
                popUp(callPostRegister(user))
            }
        }

        /* Toolbar */
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false);
        supportActionBar?.setDisplayShowHomeEnabled(false);
        tbFab = findViewById(R.id.toolbar_fabHome)
        tbFab.setImageResource(R.drawable.fab_gradient_back)
        /* Toolbar Dropdown */
        ddFab = findViewById(R.id.toolbar_fabDropdown)
        ddMenu = findViewById(R.id.toolbar_dropDownCard)

        /// Hide dd menu
        ddMenu.isVisible = false
        ddFab.isVisible = false

    }

    //gotomain
    fun goToEventFeed(view: View) {
        val intent = Intent(this, MainActivity::class.java)
//        Toast.makeText(this, "going to event feed activity", Toast.LENGTH_SHORT).show()
        startActivity(intent)
    }

    fun popUp(pop : Boolean) {

        val rootView = findViewById<View>(android.R.id.content)
        val message : String
        val duration = Snackbar.LENGTH_LONG

        if (pop) {
           message  = "Usuario creado con exito"
        } else {
            message = "Fallo al crear usuario"
        }

        val snackbar = Snackbar.make(rootView, message, duration)

        snackbar.show()
    }

}