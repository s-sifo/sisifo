package com.example.ve_merged_v1

import com.example.ve_merged_v1.dataStructure.Event
import com.example.ve_merged_v1.dataStructure.User


class GlobalPreferences {

    companion object {
        @Volatile
        private var instance : GlobalPreferences? = null

        fun getInstance() : GlobalPreferences {
            if (instance == null){
                synchronized(this) {
                    if (instance == null){
                        instance = GlobalPreferences()
                    }
                }
            }
            return instance!!
        }
    }


    lateinit var logedUser : User


}