package com.example.ve_merged_v1.eventsVh

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ve_merged_v1.R
import com.example.ve_merged_v1.dataStructure.Event


class EventsRvAdapter(private val events: List<Event>) : RecyclerView.Adapter<EventsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        val layoutInflate = LayoutInflater.from(parent.context)
        return EventsViewHolder(layoutInflate.inflate(R.layout.event_rv_layout, parent, false))
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        holder.printEvent(events[position])

    }

    override fun getItemCount(): Int {
        return events.size
    }

}