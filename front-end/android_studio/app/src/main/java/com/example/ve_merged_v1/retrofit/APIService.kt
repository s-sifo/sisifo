package com.example.ve_merged_v1.retrofit

import com.example.ve_merged_v1.dataStructure.Event
import com.example.ve_merged_v1.dataStructure.User
import com.example.ve_merged_v1.retrofit.dataSend.*
import retrofit2.Call
import retrofit2.http.*


interface APIService {

    @Headers("Accept: application/json",
        "Content-Type: application/json")
    @POST("SignUp")
    fun postRegister(@Body user: SendRegisterUser) : Call<Void>

    @POST("login")
    fun postLogin(@Body user : SendLoginUser) : Call<User>

    @POST("update")
    fun postUpdateUser(@Body user : User) : Call<User>

    @POST("CrearEvento/{userId}")
    fun postSetEvent(@Path("userId") userId: Int, @Body event: SendSendEvent) : Call<Void>

    @POST("Eventos")
    fun postGetEvent() : Call<List<Event>>

    @PUT("verificar")
    fun postVerificar(@Body id : Int): Call<Void>



    /*---POR-HACER-----

    crear evento

     */
}